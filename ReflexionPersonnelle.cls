\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ReflexionPersonnelle}[07/08/2022]

\LoadClass{report}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}

\ProcessOptions\relax

\RequirePackage[french]{babel}%ça va devenir une option de classe babel=french
\RequirePackage[utf8x]{inputenc}
\RequirePackage[T1]{fontenc}

\RequirePackage[top=3cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\RequirePackage{etoolbox}
\RequirePackage{enumitem}
\RequirePackage{graphicx}
\RequirePackage{xcolor}

\RequirePackage{hyperref}

%commande utile
\def\nothing#1{#1}

%emplacement fihciers
\def\pathS{Sections}
\def\pathC{Chapitres}

%dates
\def\JMY#1#2#3{%date au format JJ/MM/20YY:#1=JJ,#2=MM,#3=YY
#1/#2/20#3
}
\def\JMYdf#1#2#3#4#5#6{%dates de debut \`a fin date(d)-date(f)
\ifnumcomp{#3}{=}{#6}{%si meme annee
\ifnumcomp{#2}{=}{#5}{%si meme mois
\ifnumcomp{#1}{=}{#4}{%si meme jour
#4/#5/20#6
}{
#1-#4/#5/20#6
}}{
#1/#2-#4/#5/20#6
}}{
#1/#2/20#3-#4/#5/20#6
}
}

%ajout donn\'ees de section
\def\SNA#1#2#3{%ajout de materiel (qui sera comptabilise dans les dates, #1-3 date en jj/mm/20yy
\def\SJd{#1}\def\SMd{#2}\def\SYd{#3}%ecrit la date de debut
\def\SJf{#1}\def\SMf{#2}\def\SYf{#3}%et la date de fin
\def\SA##1##2##3{%apres la premiere fois
\def\SJf{##1}\def\SMf{##2}\def\SYf{##3}}%update la date de fin
}
%
\def\SNAttl#1#2#3#4{%#1-3 date #4 titre
%le N pour new vient de ce qu'il faut le redefinir a chaque section
\def\Sttl{#4}%met \`a jour le titre
\def\SJttl{#1}\def\SMttl{#2}\def\SYttl{#3}%et la date d'attribution
\def\SAttl##1##2##3##4{%apres la premiere fois
\appto\SHttl{\subsubsection{Anciens titres.}\noindent}%stocke la creation d'une soussoussection paragraphe dans le hook
\eappto\SHttl{\noexpand\emph{\JMY\SJttl\SMttl\SYttl :} \Sttl}%ajoute la pr\'esentation de l'ancien titre au hook
\def\Sttl{##4}%
\def\SJttl{##1}%
\def\SMttl{##2}%
\def\SYttl{##3}%
\def\SAttl####1####2####3####4{%apres la deuxieme fois
\eappto\SHttl{\noexpand\newline\noexpand\emph{\JMY\SJttl\SMttl\SYttl :} \Sttl}%ajoute l'ancien titre au hook
\def\Sttl{####4}%
\def\SJttl{####1}\def\SMttl{####2}\def\SYttl{####3}%
}}}
%
\def\SNActxt#1#2#3#4{%#1-3 date #4 contexte
%le N pour new vient de ce qu'il faut le redefinir a chaque section
\appto\SHctxt{\subsubsection{Contexte.}\noindent}%stocke la creation d'une soussoussection paragraphe dans le hook
\appto\SHctxt{\emph{\JMY{#1}{#2}{#3} :} #4}%ajoute le contexte au hook
\def\SActxt##1##2##3##4{%apres la premiere fois
\appto\SHctxt{\\\emph{\JMY{##1}{##2}{##3} :} ##4}%
}}
%
\def\SNAsupp#1#2#3#4{%#1-3 date #4 contexte
%le N pour new vient de ce qu'il faut le redefinir a chaque section
\appto\SHsupp{\paragraph{Support :}}%stocke la creation d'un paragraphe dans le hook
\appto\SHsupp{#4}%ajoute le support au hook
\def\SAsupp##1##2##3##4{%apres la premiere fois
\appto\SHsupp{, ##4}%
}}
%
\def\SNAdcl#1#2#3{%#date de dactylographie
%le N pour new vient de ce qu'il faut le redefinir a chaque section
\appto\SHdcl{\paragraph{Dactylographi\'e} le \JMY{#1}{#2}{#3}}%stocke la creation d'un aragraphe dans le hook
\def\SAdcl##1##2##3{%apres la premiere fois
\appto\SHdcl{, le \JMY{##1}{##2}{##3}}%
}}
%

%ajout et suppression fichiers de section
\def\SAFmain#1#2#3#4{%fichier principal (a executer)
\appto\SHmain{\Fmain{#4}}
}
\def\SAFadd#1#2#3#4{%fichier (a executer) avec texte ajouté plus tard
\appto\SHmain{\Fadd{#4}}
}
\def\SAFtemp#1#2#3#4{%fichier temporaire (en attente de dactylographie)
\appto\SHmain{\Ftemp{#4}}
}
\def\SAFref#1#2#3#4{%ref a support externe (en attente de dactylographie)
\appto\SHmain{\Fref{#4}}
}
\def\SAFaux#1#2#3#4{%fichier auxiliaire (juste repertorie)
\listadd\SLFaux{#4}
}
\def\SAFess#1#2#3#4{%essentiel
\listadd\SLFess{#4}
}

%donn\'ee essentiel
\def\Eit#1#2{%item #1:niveau #2:contenu
\ifnumcomp{#1}{>}{4}{%si l'item est plus profond que 4 niveaux (max latex par defaut)
\Eit{4}{[\bf #1]#2}%remplace par un item de niveau 4 qui porte la profondeur voulue comme label
}{
\whileboolexpr{test{\ifnumcomp{\Elvl}{<}{#1}}}{%tant que le niveau actuel est inferieur a celui de l'item
\item[]%item vide pour eviter itemize sans item
\begin{itemize}[label=$\bullet$]
\numgdef\Elvl{\Elvl+1}%incremente niveau actuel car nouvel itemize
}
\whileboolexpr{test{\ifnumcomp{\Elvl}{>}{#1}}}{%meme chose si le niveau actuel est trop haut
\end{itemize}
\numgdef\Elvl{\Elvl-1}
}
\item#2%
}}

%donnees chapitre
\def\CAS#1#2#3#4#5{%ajoute section
\listadd\CLS{{#4}{#5}}
}
%
\def\CNAttl#1#2#3#4{%#1-3 date #4 titre
\def\Cttl{#4}%met \`a jour le titre
\def\CJttl{#1}\def\CMttl{#2}\def\CYttl{#3}%et la date d'attribution
\def\CAttl##1##2##3##4{%apres la premiere fois
\appto\CHttl{\subsubsection{Anciens titres.}\noindent}%stocke la creation d'une soussoussection dans le hook
\eappto\CHttl{\noexpand\emph{\JMY\CJttl\CMttl\CYttl :} \Cttl}%ajoute la pr\'esentation de l'ancien titre au hook
\def\Cttl{##4}%
\def\CJttl{##1}%
\def\CMttl{##2}%
\def\CYttl{##3}%
\def\CAttl####1####2####3####4{%apres la deuxieme fois
\eappto\CHttl{\noexpand\newline\noexpand\emph{\JMY\CJttl\CMttl\CYttl :} \Cttl}%ajoute l'ancien titre au hook
\def\Cttl{####4}%
\def\CJttl{####1}\def\CMttl{####2}\def\CYttl{####3}%
}}}
%
\def\CNActxt#1#2#3#4{%#1-3 date #4 contexte
%le N pour new vient de ce qu'il faut le redefinir a chaque section
\appto\CHctxt{\subsubsection{Contexte.}\noindent}%stocke la creation d'une soussoussection paragraphe dans le hook
\appto\CHctxt{\emph{\JMY{#1}{#2}{#3} :} #4}%ajoute le contexte au hook
\def\CActxt##1##2##3##4{%apres la premiere fois
\appto\CHctxt{\\\emph{\JMY{##1}{##2}{##3} :} ##4}%
}}

%execution de section
\def\Sexe#1#2{%
%on stocke la ref de la section en cours
\def\SYs{#1}
\def\SMJs{#2}
%on reinitialise les donnees
\def\SJd{}%jour de debut
\def\SMd{}%mois
\def\SYd{}%annee
\def\SJf{}%jour de fin
\def\SMf{}%
\def\SYf{}%
\def\Sttl{}%titre
\def\SJttl{}%Jour d ecriture du titre
\def\SMttl{}%Mois d ecriture du titre
\def\SYttl{}%Year d ecriture du titre
%et les hooks
\def\SHttl{}%anciens titres
\def\SHctxt{}%contextes
\def\SHsupp{}%supports
\def\SHdcl{}%dactylographie
\def\SHmain{}%texte principal
%et les listes
\def\SLFaux{}%fichiers auxiliaires
\def\SLFess{}%fichiers essentiel
%et les commandes d'ajout mutables
\let\SA\SNA
\let\SAttl\SNAttl
\let\SActxt\SNActxt
\let\SAsupp\SNAsupp
\let\SAdcl\SNAdcl
%
\input{\pathS/#1/#2/.hist}%charge le fichier historique
\vspace{.5cm}
\par\noindent\framebox{%
  \large\href{run:\pathS/#1/#2/.hist}{#1.#2}
}
\vspace{-.5cm}
\addcontentsline{toc}{section}{\textbf{\SYs.\SMJs} \Sttl\hfil\hbox{}\protect\linebreak\textit{\JMYdf\SJd\SMd\SYd\SJf\SMf\SYf}}
\label{sec:#1.#2}%labelle la section avec la reference
\section*{\Sttl}%cr\'e\'e une section avec le titre
\paragraph{Dates : }\JMYdf\SJd\SMd\SYd\SJf\SMf\SYf
%
\ifdefempty\Sttl{}{%si le titre actuel existe
\ifdefempty\SYttl{}{%et si son annee d'attribution existe
\paragraph{Titre actuel : }\JMY\SJttl\SMttl\SYttl%alors on la donne
}}
%
\SHttl%applique le hook des anciens titres
\SHsupp%et des supports
\SHdcl%et de la dactylographie
\SHctxt%et des contextes
\forlistloop\Fess\SLFess
\def\endFaux{}
\let\Faux\FauxN
\forlistloop\Faux\SLFaux
\endFaux
\SHmain
}

%execution de fichiers
\def\Fmain#1{%
\subsubsection{\href{run:\pathS/\SYs/\SMJs/#1.tex}{#1}}\noindent
\input{\pathS/\SYs/\SMJs/#1.tex}
}
\def\Fadd#1{%
{\color{orange}
\subsubsection{\href{run:\pathS/\SYs/\SMJs/#1.tex}{#1}}\noindent
\input{\pathS/\SYs/\SMJs/#1.tex}
}}
\def\Ftemp#1{%
\subsubsection{\href{run:\pathS/\SYs/\SMJs/#1}{#1}}\noindent
\emph{(à latexer)}
}
\def\Fref#1{%
\subsubsection{#1}\noindent
\emph{(à dactylographier)}
}
\def\Fess#1{%
\subsubsection{Essentiel}\noindent
\begin{itemize}
\numgdef\Elvl{1}%cree une variable globale pour compter les niveaux,
%l'initialise a 1,
%def globale car sinon l'incrementation avant \end{itemize} plante
\input{\pathS/\SYs/\SMJs/#1.tex}
\whileboolexpr{test{\ifnumcomp{\Elvl}{>}{1}}}{%on revient au niveau 1
\end{itemize}
\numgdef\Elvl{\Elvl-1}
}
\end{itemize}
}
\def\FauxN#1{%
\subsubsection{Fichiers auxiliaires.}
\def\endFaux{\end{itemize}}
\begin{itemize}
\item\href{run:\pathS/\SYs/\SMJs/#1}{#1}
\def\Faux##1{%
\item\href{run:\pathS/\SYs/\SMJs/##1}{##1}
}
}

%execute chapitre
\def\Cexe#1{
%on commence par initialiser les donnees
\def\Cttl{}%titre
\def\CJttl{}%Jour d ecriture du titre
\def\CMttl{}%Mois d ecriture du titre
\def\CYttl{}%Year d ecriture du titre
%et les hooks
\def\CHttl{}%anciens titres
\def\CHctxt{}%contextes
%et les listes
\def\CLS{}
%et les commendes d'ajout mutables
\let\CAttl\CNAttl
\let\CActxt\CNActxt
%
\input{\pathC/#1.tex}%on execute le fichier
\chapter{\Cttl}%cr\'e\'e un chap avec le titre
\label{chap:#1}%labelle le chap avec la reference
\paragraph{R\'ef\'erence : }\href{run:\pathC/#1.tex}{#1}
%
\ifdefempty\Cttl{}{%si le titre actuel existe
\ifdefempty\CYttl{}{%et si son annee d'attribution existe
\paragraph{Titre actuel : }\JMY\CJttl\CMttl\CYttl%alors on la donne
}}
%
\CHttl%hook des anciens titres
\CHctxt%contextes
\forlistloop{\expandafter\Sexe\nothing}\CLS
}

\RequirePackage[normalem]{ulem}
\newcommand{\refC}[1]{%
  \@ifundefined{r@chap:#1}{%
    \sout{\href{run:\pathC/#1.tex}{#1}}%
  }{%
    \ref{chap:#1}%
  }
}
\newcommand{\refS}[2]{%
  \@ifundefined{r@sec:#1.#2}{%
    \sout{#1.#2}%
  }{%
    \hyperref[sec:#1.#2]{#1.#2}%
  }
}
\newcommand{\hrefS}[3]{\hyperref[sec:#1.#2]{#3}}
\newcommand{\refF}[1]{\href{run:\pathS/\SYs/\SMJs/#1}{#1}}
\newcommand{\hrefF}[2]{\href{run:\pathS/\SYs/\SMJs/#1}{#2}}
\newcommand{\refFS}[3]{\href{run:\pathS/#1/#2/#3}{#3}}
\newcommand{\hrefFS}[4]{\href{run:\pathS/#1/#2/#3}{#4}}
\newcommand\refTemp[1]{\framebox{#1}}
\newcommand\hrefTemp[2]{\framebox{[#2](#1)}}

\def\Sgraphicspath{\graphicspath{{\pathS/\SYs/\SMJs/}}}%permet dechercher des images dans le dossier de la section
