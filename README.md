# ReflexionPersonnelle

:fr:
Classe LaTeX utilisée pour structurer mon journal intime.
Pour l'instant tout est en français.

L'unité de base est la section, définie par 2 chaînes de caractères `Y` et `MJ`, référencée `sec:Y.MJ` et invoquée par la commande `\Sexe{Y}{MJ}`.
Son contenu est défini par celui du fichier `\pathS/Y/MJ/.hist` ainsi que les autres fichiers du dossier `\pathS/Y/MJ`.

On peut facultativement grouper les sections en chapitres.
Chaque chapitre est défini par une chaîne de caractères `C`, référencé par `chap:C`, invoqué par la commande `\Cexe{C}` et décrit par le fichier `\pathC/C.tex`.

La branche `example` de ce dépôt contient un exemple d'usage.

:uk:
LaTeX class used to structure my diary.
So far everything is in French.

The basic unit is the Section, defined by 2 strings `Y` and `MJ`, referenced `sec:Y.MJ` and called by the command `\Sexe{Y}{MJ}`.
Its content is defined by the one of the file `\pathS/Y/MJ/.hist` as well as the one of the other files of the folder `\pathS/Y/MJ`.

One can optionnally group sections in chapters.
each chapter is defined by a string `C`, is referenced `chap:C`, is called by the command `\Cexe{C}` and is described by the file `\pathC/C.tex`.

The `example` branch of this repository contains an usage example.